package breakouttest;
import java.awt.*;

public class Paddle{
	public Color color;
	private int x, y, width, height;
	private int maxWidth;
	private int minWidth;
	
	public Paddle(int X, int Y, int Width, int Height, Color c){
		x = X;
		y = Y;
		width = Width;
		height = Height;
		color = c;
		maxWidth = width*2;
		minWidth = width/2;
	}
	public void setX(int X){
		x = X;
	}
	
	public int getY(){
		return y;
	}
	
	public int getWidth(){
		return width;
	}
	
	public void setWidth(int Width){
		if(Width > maxWidth || Width < minWidth){
			return;
		}else{
			width = Width;
		}
	}
	
	public boolean hitLeft(int X, int Y){
		if((X >= x) && (X <= x+(width/2)) && ((y == Y)||(y == Y - 1))){
			return true;
		}
		return false;
	}
	public boolean hitRight(int X, int Y){
		if(((X >= x+(width/2)) && (X <= x+width)) && ((y == Y)||(y == Y - 1))){
			return true;
		}
		return false;
	}
	
	public boolean absorbPill(Pill p){
		if((p.getX() < x + width) && (p.getX()+ p.getWidth() > x) && (y == p.getY()||y == p.getY() -1)){
			p.absorbed(this);
			return true;
		}
		return false;
	}
	
	public void draw(Graphics g)
        {
		Color oldColor = g.getColor();
		g.setColor(color);
		g.fillRect(x,y,width,height);
		g.setColor(oldColor);
	}
}