package breakouttest;

import java.awt.*;

public class Brick{
	public Color color;
	private int x, y, width, height, numHits, maxHits;
	private boolean visible;
	public Pill pill;
	
	
	public Brick(int X, int Y, int Width, int Height, int pillType, boolean Metal){
		x = X;
		y = Y;
		width = Width;
		height = Height;

		pill = new Pill(x+(width/4),y+(height/4),width/2,height/2,pillType);
		if(Metal){
			maxHits = 2;
			color = Color.GREEN;
		}else{
			maxHits = 1;
			color = Color.RED;
		}
		numHits = 0;
		visible = true;
	}
	public void setX(int X){
		x = X;
	}

	public void addHits(){
		numHits++;
		color = Color.RED;
		if(numHits == maxHits){
			visible = false;
		}
	}
	public boolean hitBottom(int X, int Y){
		if((X>=x) && (X <= x+width) && ((y+height == Y)||(y+height == Y + 1)) && visible == true){
			addHits();
			return true;
		}
		return false;
	}
	
	public void setVisible(boolean b){
		visible = b;
	}
	
	public boolean hitTop(int X, int Y){
		if((X>=x) && (X <= x+width) && ((y == Y)||(y == Y - 1)) && visible == true){
			addHits();
			return true;
		}
		return false;
	}
	
	public boolean hitLeft(int X, int Y){
		if((Y>=y) && (Y <= y+height) && ((x == X)||(x == X - 1)) && visible == true){
			addHits();
			return true;
		}
		return false;
	}
	
	public boolean hitRight(int X, int Y){
		if((Y>=y) && (Y <= y+height) && ((x+width == X)||(x+width == X - 1)) && visible == true){
			addHits();
			return true;
		}
		return false;
	}
	
	public boolean destroyed(){
		return !visible;
	}
	
	public void draw(Graphics g){
		if(visible){
			Color oldColor = g.getColor();
			g.setColor(color);
			g.fillRect(x,y,width,height);
			g.setColor(oldColor);
		}
	}
	
}
