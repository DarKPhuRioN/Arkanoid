package breakouttest;

import java.awt.*;

public class Pill
{
	public static final int BIG = 1;
	public static final int SMALL = 2;
	public static final int NOTHING = 3;
	
	int x, y,width,height,type;
	
	public Pill(int X, int Y,int Width, int Height, int Type){
		x = X;
		y = Y;
		width = Width;
		height = Height;
		type = Type;
	}
	
	public void drop(){
		y += 1;
	}
	public int getY(){
		return y;
	}
	public int getX(){
		return x;
	}
	public int getWidth(){
		return width;
	}
	public int getType(){
		return type;
	}
	
	public void absorbed(Paddle p)
        {
		if(getType() == BIG)
                {
			p.setWidth(p.getWidth() * 2);
		}else if(getType() == SMALL){
			p.setWidth(p.getWidth()/2);
		}
	}
	public void draw(Graphics g){
		Color oldColor = g.getColor();
		if(type == NOTHING){
			return;
		}else if(type == BIG){
			g.setColor(Color.BLUE);
		}else if(type == SMALL){
			g.setColor(Color.MAGENTA);
		}
		g.fillRect(x,y,width,height);
		g.setColor(oldColor);
	}
}