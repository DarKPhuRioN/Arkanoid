package breakouttest;

import javax.swing.*;
import java.awt.*;

public class Ball{
	private int xCoord, yCoord, radius;
	private Color color;
	private int xDir = 1, yDir = 2 ;
	
	public Ball(int x, int y , int r, int xD, int yD, Color c){
		xCoord = x;
		yCoord = y;
		setRadius(r);
		setColor(c);
	}
	public int getX(){
		return xCoord;
	}
	public int getY(){
		return yCoord;
	}
	public void setY(int y){
		yCoord = y;
	}
	public int getRadius(){
		return radius;
	}
	public Color getColor(){
		return color;
	}
	public void setXDir(int x){
		xDir = x;
	}
	public void setYDir(int y){
		yDir = y;
	}
	
	public int getXDir(){
		return xDir;
	}
	public int getYDir(){
		return yDir;
	}
	public void setRadius(int r){
		radius = r;
	}
	public void setColor(Color c){
		color = c;
	}
	public void move(){
		xCoord = xCoord + xDir;
		yCoord  = yCoord + yDir;
	}
	public void draw(Graphics g){
		Color oldColor = g.getColor();
		g.setColor(color);
		g.fillOval(xCoord - radius, yCoord - radius, radius * 2, radius * 2 );
		g.setColor(oldColor);
	}
	public boolean containsPoint(int x, int y){
		int xSquared = (x-xCoord)*(x - xCoord);
		int ySquared = (y - yCoord)*(y - yCoord);
		int radiusSquared = radius *radius;
		return xSquared + ySquared - radiusSquared <= 0;
	}
}